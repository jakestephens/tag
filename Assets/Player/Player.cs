﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public Light light;
	
	private CharacterController controller;
	private bool isIt = false;

	void Start()
	{
		controller = GetComponent<CharacterController>();
		light.enabled = false;
	}
	
	void FixedUpdate()
	{
		float rotate = Mathf.Clamp(Input.GetAxis("Horizontal"), -5f, 5f);
		transform.Rotate(new Vector3(0, rotate * 2, 0));
		
		float movement = Mathf.Clamp(Input.GetAxis("Vertical"), -0.25f, 0.25f);
		controller.Move(transform.forward * movement);
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject.name.Contains("skeleton"))
		{
			isIt = !isIt;
			light.enabled = !light.enabled;

		}
	}
}
