﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour
{

	void Start()
	{	
		//tile the textures based on the size of the cube
		Mesh m = GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = m.vertices;
		Vector2[] uvCoords = m.uv;
		
		//increment x by face
		for(int x = 0; x < uvCoords.Length; x = x + 4)
		{	
			//and increment y by for the verts in that face
			for(int y = 0; y < 4; y++)
			{
				//if the face does not vary in z, tile it by the scale in x
				if(vertices[x].z == vertices[x+1].z && vertices[x].z == vertices[x+2].z && vertices[x].z == vertices[x+3].z)
				{
					uvCoords[x+y].x = uvCoords[x+y].x * transform.localScale.x * 2;
				}
				//otherwise, tile it by the scale in z
				else
				{
					uvCoords[x+y].x = uvCoords[x+y].x * transform.localScale.z * 2;		
				}
				uvCoords[x+y].y = uvCoords[x+y].y * 2f;
			}
		}
		
		GetComponent<MeshFilter>().mesh.uv = uvCoords;
	}
	
	void Update()
	{
	
	}
}
