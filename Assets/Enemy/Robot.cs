﻿using UnityEngine;
using System.Collections;

public class Robot : MonoBehaviour
{
	public Transform playerObject;
	public Light light;
	public GameObject[] navTargets;
	
	public GameObject visibilityIndicator;
	public Material spotted;
	public Material lost;
	public Material hidden;
	public Material it;
	
	private bool isIt = true;
	private NavMeshAgent thisRobot;
	private Vector3 lastKnownPos = new Vector3(0, -10f, 0);
	private int currentTarget;
	private Vector3 lastPos;
	
	void Start()
	{
		light.enabled = true;
		thisRobot = GetComponent<NavMeshAgent>();
	}
	
	void FixedUpdate()
	{	
		//use the enemy's speed to show the correct animation
		if(thisRobot.velocity.magnitude < 0.01f)
		{
			GetComponent<Animation>().Play("idle");
		}
		else
		{
			GetComponent<Animation>().Play("run");
		}
		
		if(isIt)
		{
			//determine if the robot has line of sight to the player
			NavMeshHit obstruction;
			bool lineOfSight = !thisRobot.Raycast(playerObject.position, out obstruction);
			
			//if it does not, but the obstructor is very close to the player, assume line of sight
			//raycasting appears to be a little bit sensitive
			if(!lineOfSight && (obstruction.position - playerObject.position).magnitude < 5f)
			{
				lineOfSight = true;	
			}
			
			//if the robot has line of sight and is more-or-less facing the player, get 'em
			if(lineOfSight && Vector3.Dot(transform.forward, (playerObject.position - transform.position).normalized) >= -0.25f)
			{
				visibilityIndicator.GetComponent<Renderer>().material = spotted;
				thisRobot.destination = playerObject.position;
				thisRobot.speed = 13;
				light.color = Color.red;
				lastKnownPos = playerObject.position;
			}
			//if the robot SAW the player, go to the player's last known position
			else if(lastKnownPos.y > -1f && (lastKnownPos - thisRobot.transform.position).magnitude > 5f)
			{
				visibilityIndicator.GetComponent<Renderer>().material = lost;
				thisRobot.destination = lastKnownPos;
			}
			//otherwise, choose a random nav target and go to that
			else
			{
				visibilityIndicator.GetComponent<Renderer>().material = hidden;
				light.color = Color.yellow;
				lastKnownPos = new Vector3(0, -10f, 0);
				
				//choose a new target once you arrive at the last target
				if((navTargets[currentTarget].transform.localPosition - thisRobot.transform.position).magnitude < 5f)
				{
					currentTarget = Random.Range(0, navTargets.Length);
				}
				
				thisRobot.destination = navTargets[currentTarget].transform.localPosition;
				thisRobot.speed = 9;
			}
			
		}
		else
		{
			thisRobot.speed = 13;
			visibilityIndicator.GetComponent<Renderer>().material = it;
			
			//determine the "cost" of each nav point
			//low cost = high distance from player, short distance from robot
			float lowestCost = 1000f;
			int lowestCostIndex = 0;
			for(int x = 0; x < navTargets.Length; x++)
			{
				float distanceFromRobot = (navTargets[x].transform.localPosition - transform.localPosition).magnitude;
				float distanceToPlayer = (navTargets[x].transform.localPosition - playerObject.localPosition).magnitude;
				
				//avoid targeting points that would cause the robot to go towards the player
				Vector3 vecToNavPoint = (navTargets[x].transform.localPosition - transform.localPosition).normalized;
				Vector3 vecToPlayer = (playerObject.localPosition - transform.localPosition).normalized;
				float directionCostModifier = Vector3.Dot(vecToNavPoint, vecToPlayer) * 75;
				
				float tempCost = distanceFromRobot - (distanceToPlayer * 2f) + directionCostModifier;
				if(tempCost < lowestCost)
				{
					lowestCostIndex = x;
					lowestCost = tempCost;
				}
			}
			
			//go to nav target with the lowest cost
			GetComponent<NavMeshAgent>().destination = navTargets[lowestCostIndex].transform.localPosition;
		}
	}
	
	void OnTriggerEnter(Collider c)
	{
		//when the robot collides with the player, toggle its light and "it" status
		if(c.gameObject.name.Contains("Player"))
		{
			isIt = !isIt;
			light.enabled = !light.enabled;
		}
	}
}
